#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    @author: xeranas <xeranas@gmail.com>
#    @license: GPL3 (see attached LICENSE.txt)
#    @homepage: https://github.com/xeranas/gray-snake 

import grid
import food
import snake

WINDOW_HEIGHT = 400
WINDOW_WIDTH = 600
WINDOW_BORDER_SIZE = 20
WINDOW_BORDER_COLOR = 59, 63, 61
PLAYGROUND_HEIGHT = WINDOW_HEIGHT - 2*WINDOW_BORDER_SIZE
PLAYGROUND_WIDTH = WINDOW_WIDTH - 2*WINDOW_BORDER_SIZE
PLAYGROUND_COLOR = 118, 120, 119

GRIDCELL_SIZE = 20
GRIDCELL_BORDER = 1
GRIDCELL_COLOR = 158, 160, 159

SNAKE_START_LENGHT = 3
SNAKE_LIVES = 3
SNAKE_SPEED = 0.1
#GCI stand for Grid Cell Index
SNAKE_GCI_X = 9 
SNAKE_GCI_Y = 14
SNAKE_COLOR = 44, 47, 45
SNAKE_DIRECTION = [0,-1]
FOOD_COLOR = 206, 207, 207
TEXT_COLOR = 158, 160, 159

ground = grid.Grid(WINDOW_BORDER_SIZE, WINDOW_BORDER_SIZE, 
                        PLAYGROUND_WIDTH, PLAYGROUND_HEIGHT, GRIDCELL_SIZE,
                        GRIDCELL_COLOR, GRIDCELL_BORDER)
#here GRID maded just for shorcut to access main game grid
GRID = ground.getGrid()
regurlarFood = food.Food(ground, (206, 207, 207))
playerSnake = snake.Snake(ground, SNAKE_LIVES, SNAKE_SPEED, SNAKE_START_LENGHT, 
                            (SNAKE_GCI_X, SNAKE_GCI_Y), SNAKE_COLOR)
