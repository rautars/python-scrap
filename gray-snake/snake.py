#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    @author: xeranas <xeranas@gmail.com>
#    @license: GPL3 (see attached LICENSE.txt)
#    @homepage: https://github.com/xeranas/gray-snake 

class Snake():
    def __init__(self, ground, lifes, speed, lenght,
                 position, color, border=0, score=0):
        self.ground = ground 
        self.color = color
        self.border = border
        self.score = score
        self.lifes = lifes
        self.initSpeed = float(speed)
        self.speed = float(speed)
        self.foot_progress = 0.0
        self.initialLenght = lenght
        self.lenght = 3

        #current possible states: ALIVE; DEAD
        self.state = 'ALIVE'

        self.initRowPos, self.initCollPos = position
        self.initStartPosition()
        self.direction = [0, 0]
        self.pendingDirection = [0, 0]
    def initStartPosition(self):
        self.resetTailLenght()
        self.resetSpeed()
        self.rowPos, self.collPos = self.initRowPos, self.initCollPos
        self.tail = [self.ground.getGrid()[self.rowPos][self.collPos]]
    def resetTailLenght(self):
        self.lenght = self.initialLenght
    def resetSpeed(self):
        self.speed = self.initSpeed
    def check_state(self, cell):
        if cell.status == 'FOOD':
            self.lenght += 1
            self.score += int(100 * self.speed)
            self.speedUp()
            cell.objRef.spawn()
    def update(self, cell):
        self.tail.insert(0, cell)
        self.check_state(cell)
        if cell.status == 'SNAKE' and self.direction != [0, 0]:
            self.state = 'DEAD'
            self.pendingDirection = [0, 0]
            return None
        cell.placeSnake(self)
        while len(self.tail) > self.lenght:
            self.tail.pop().clear()
        for cell in self.tail:
            cell.placeSnake(self)
    def autoUpdate(self):
        self.direction = self.pendingDirection

        if (self.foot_progress >= 1):
            self.rowPos += self.direction[0]
            self.collPos += self.direction[1]

            #here '-1' due index starts from 0
            if (self.rowPos < 0 or self.collPos < 0 or 
                self.rowPos > self.ground.rows-1 or 
                self.collPos > self.ground.colls-1):
                self.state = 'DEAD'
                self.pendingDirection = [0, 0]
                return None
            self.update(self.ground.getGrid()[self.rowPos][self.collPos])

            self.foot_progress = 0.0
        else:
            self.foot_progress += self.speed
            self.check_state(self.ground.getGrid()[self.rowPos][self.collPos])
    def loseLife(self):
        self.lifes -= 1
    def speedUp(self):
        self.speed += 0.07 
    def getSpeed(self):
        return int(self.speed)
    def keyDown(self):
        if self.direction[0] == 0:
            self.pendingDirection = [1,0]
        return None
    def keyUp(self):
        if self.direction[0] == 0:
            self.pendingDirection = [-1,0]
        return None
    def keyRight(self):
        if self.direction[1] == 0:
            self.pendingDirection = [0,1]
        return None
    def keyLeft(self):
        if self.direction[1] == 0:
            self.pendingDirection = [0,-1]
        return None
