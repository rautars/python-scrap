#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    @author: xeranas <xeranas@gmail.com>
#    @license: GPL3 (see attached LICENSE.txt)
#    @homepage: https://github.com/xeranas/gray-snake 

import pygame
import time
import sys
import copy
import glob
import snake
import food
import os
import platform
if platform.system() == 'Windows':
    os.environ['SDL_VIDEODRIVER'] = 'windib'
    
pygame.init()

pygame.display.set_icon(pygame.image.load('graySnake.png'))
pygame.display.set_caption('Gray Snake', 'graySnake.png')
font = pygame.font.Font('Ubuntu-R.ttf', 16)
screen = pygame.display.set_mode((glob.WINDOW_WIDTH, glob.WINDOW_HEIGHT))
clock = pygame.time.Clock()

def displayScore(score):
    renderedText = font.render('Score: '
        +str(score), True, glob.TEXT_COLOR)
    screen.blit(renderedText, (glob.WINDOW_BORDER_SIZE,2))

def displayLifes(lifes):
    renderedText = font.render('Lifes: '+str(lifes), True, glob.TEXT_COLOR)
    screen.blit(renderedText, (glob.WINDOW_WIDTH-72,2))

def displayMessage(text):
    renderedText = font.render(text, True, glob.TEXT_COLOR)
    textPos = renderedText.get_rect(centerx=glob.WINDOW_WIDTH/2)
    screen.blit(renderedText, textPos)

def waitForInput(text):
    glob.ground.draw(pygame.draw, screen) 
    displayScore(glob.playerSnake.score)
    displayLifes(glob.playerSnake.lifes)
    displayMessage(text)
    pygame.display.update()
    wait = True
    while wait:
        time.sleep(1)
        clock.tick(0)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                wait = False

def gameloop():
    screen.fill(glob.WINDOW_BORDER_COLOR)
    pygame.draw.rect(screen, glob.PLAYGROUND_COLOR,
                     ((glob.WINDOW_BORDER_SIZE, glob.WINDOW_BORDER_SIZE),
                     (glob.PLAYGROUND_WIDTH, glob.PLAYGROUND_HEIGHT)))
    currentMessage = 'Use arrows to control snake'
    glob.regurlarFood.spawn()
    while True:
        clock.tick(60)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                currentMessage = ''
                if event.key == pygame.K_DOWN:
                    glob.playerSnake.keyDown()
                if event.key == pygame.K_UP:
                    glob.playerSnake.keyUp()
                if event.key == pygame.K_RIGHT:
                    glob.playerSnake.keyRight()
                if event.key == pygame.K_LEFT:
                    glob.playerSnake.keyLeft()

        screen.fill(glob.WINDOW_BORDER_COLOR)
        pygame.draw.rect(screen, glob.PLAYGROUND_COLOR,
                         ((glob.WINDOW_BORDER_SIZE, glob.WINDOW_BORDER_SIZE),
                         (glob.PLAYGROUND_WIDTH, glob.PLAYGROUND_HEIGHT)))

        glob.playerSnake.autoUpdate()
        if (glob.playerSnake.state == 'DEAD'):
            glob.playerSnake.loseLife()
            waitForInput('OOUCH.. Press any key to continue')
            glob.ground.clear()
            if glob.playerSnake.lifes == 0:
                screen.fill(glob.WINDOW_BORDER_COLOR)
                pygame.draw.rect(screen, glob.PLAYGROUND_COLOR,
                                 ((glob.WINDOW_BORDER_SIZE, glob.WINDOW_BORDER_SIZE),
                                 (glob.PLAYGROUND_WIDTH, glob.PLAYGROUND_HEIGHT)))
                waitForInput('You lose last life. Press any key to restart game')
                glob.playerSnake.lifes = 3
                glob.playerSnake.score = 0
            glob.playerSnake.state = 'ALIVE'
            glob.playerSnake.initStartPosition() 
            glob.regurlarFood.spawn()
            currentMessage = ''

        glob.ground.draw(pygame.draw, screen) 
        displayScore(glob.playerSnake.score)
        displayLifes(glob.playerSnake.lifes)
        displayMessage(currentMessage)
        pygame.display.update()
    return 0

if __name__ == '__main__':
    gameloop()
