#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    @author: xeranas <xeranas@gmail.com>
#    @license: GPL3 (see attached LICENSE.txt)
#    @homepage: https://github.com/xeranas/gray-snake 

from cell import Cell
from random import choice

class Grid():
    def __init__(self, x, y, width, height, cellSize, cellColor, cellBorder):
        self.width, self.height = width, height
        self.x, self.y = x, y
        self.cellSize = cellSize
        self.rows = int(self.height / self.cellSize)
        self.colls = int(self.width / self.cellSize)
        self.grid = self.generateGrid(cellColor, cellBorder)
    def generateGrid(self, color, border):
        rows = []
        cellCoorY = self.y - self.cellSize
        for i in range(self.rows):
            cellCoorY += self.cellSize
            cellCoorX = self.x - self.cellSize
            colls = []
            for ii in range(self.colls):
                cellCoorX += self.cellSize
                colls.append(Cell((cellCoorX, cellCoorY), color, border))
            rows.append(colls)
        return tuple(rows)

    def returnRandomCellByStatus(self, status):
        return choice(self.searchByStatus(status))        

    def searchByStatus(self, status):
        statusCoors = []
        for row in self.grid:
            for coll in coll:
                if self.grid[row][coll].status == status:
                    statusCoors.append(self.grid[row][coll]) 
        return statusCoors

    def getGrid(self):
        return self.grid

    def clear(self):
        for gridRow in self.grid:
            for cell in gridRow:
                cell.clear()

    def draw(self, pygameDrawObj, screen):
        for gridRow in self.grid:
            for cell in gridRow:
                pygameDrawObj.rect(screen, cell.color,
                            (cell.x, cell.y, self.cellSize, self.cellSize),
                            cell.border) 

