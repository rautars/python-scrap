#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#    @author: xeranas <xeranas@gmail.com>
#    @license: GPL3 (see attached LICENSE.txt)
#    @homepage: https://github.com/xeranas/gray-snake 

from random import choice

class Food():
    def __init__(self, ground, color, border = 0, speedBonus = 0):
        self.ground = ground
        self.color = color
        self.border = border
        self.speedBonus = speedBonus
    def spawn(self):
        emptyCells = []
        for gridRow in self.ground.getGrid():
            for gridColl in gridRow:
                if gridColl.status == 'FREE':
                    emptyCells.append(gridColl)
        choice(emptyCells).placeFood(self)

