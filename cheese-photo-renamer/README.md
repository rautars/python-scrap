#Cheese Photo Renamer

##About
Simple python script to rename multiple images. Created mainly for learning purpose.

##Usage
* `cheese-photo-renamer.py --help` will print script usage instructions.

* `cheese-photo-renamer.py [image-folder-dir]` will run script on specified directory.

##License
Script is under MIT license (read LICENSE file).
