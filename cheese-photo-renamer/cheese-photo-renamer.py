#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

def getZeros(n):
    zeros = ''
    for i in range(n):
        zeros += '0'
    return zeros    

def main(dir_from = '.'):
    """
    Simple script which rename images.
    Script idea is extract number from filename and write 
    in proper format.
    
    E.g: 2012-04-22-104742_984.jpg > 000984.jpg
    
    Script can take only one optional parameter and it 
    will interpretate as directory (except for --help,-help -h) 
    """

    # here can be change variables to suit your needs
    maxZeros = 9 # number of zeros reserved (with 9 numbering begins 000000001)
    patt1 = '^.*_' # pattern to indicate first part of filename (before numbers)
    patt2 = '\..*' # pattern to indicate last part of filename (after numbers)
    os.chdir(dir_from)

    for filename in os.listdir('.'):
        oldName = filename
        ext = os.path.splitext(filename)[1]
        filename = re.sub(patt1, '', filename)
        filename = re.sub(patt2, '', filename)
        zeroCounter = 0
        try:
            for i in range(1, maxZeros):
                number = int('1' + str(getZeros(i)))
                fileNumber = int(filename)
                if fileNumber < number:
                   zeroCounter += 1 
            newName = str(getZeros(zeroCounter)) + str(filename) + ext
            os.rename(oldName, newName)
        except:
            print 'Notice: unable change filename: ' + oldName + '\n'

if __name__ == "__main__":
    if sys.argv[1] == '--help' or sys.argv[1] == '-help' or sys.argv[1] == '-h':
        sys.exit(main.__doc__)
    try:
        directory = sys.argv[1]
    except IndexError:
    	directory = '.'
    main(directory)
